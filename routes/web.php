<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/create', function () use ($router) {


    $userFirst = factory(\App\User::class)->create(['password' => 'Some pass']);
    $userSecond = factory(\App\User::class)->create(['password' => 'Some pass']);

    $article = $userFirst->articles()->create([
        'title' => 'Lorem',
        'body' => 'Ipsum',
    ]);

    $userFirst->address()->create([
        'city' => 'Klaipeda',
        'street' => 'Memelio',
    ]);

    $article->comments()->create([
        'user_id' => $userSecond->id,
        'body' => 'Comment body',
    ]);

    return response('created');
});

//users
//users?include=articles
//users?include=articles,address
//users?include=articles,address.comments
//users?include=articles,address.comments.user
$router->get('/users', function (\Illuminate\Http\Request $request) use ($router) {
    //DB::connection()->enableQueryLog();

    $user = \App\User::with(['articles.comments.user', 'address'])->first();

    $params = new \Neomerx\JsonApi\Encoder\Parameters\EncodingParameters(
        $request->input('include') ? (explode(',', $request->input('include'))) : []
    );

    $encoder = \Neomerx\JsonApi\Encoder\Encoder::instance([
        \App\User::class => \App\Schemas\UserSchema::class,
        \App\Article::class => \App\Schemas\ArticleSchema::class,
        \App\Address::class => \App\Schemas\AddressSchema::class,
        \App\Comment::class => \App\Schemas\CommentSchema::class
    ], new \Neomerx\JsonApi\Encoder\EncoderOptions(JSON_PRETTY_PRINT));

    //var_dump(DB::getQueryLog());

    return $encoder->encodeData($user, $params);
});
<?php

namespace App\Schemas;

use Neomerx\JsonApi\Schema\SchemaProvider;

class CommentSchema extends SchemaProvider
{
    protected $resourceType = 'comment';

    public function getId($comment)
    {
        return $comment->id;
    }

    public function getAttributes($comment)
    {
        return [
            'body' => $comment->title,
            'user_id' => $comment->user_id,
        ];
    }

    public function getRelationships($comment, $isPrimary, array $includeList)
    {
        $return = [];
        if (in_array('user', $includeList)) {
            $return['user'][self::DATA] = $comment->user;
        }
        return $return;
    }
}
<?php

namespace App\Schemas;

use Neomerx\JsonApi\Schema\SchemaProvider;

class UserSchema extends SchemaProvider
{
    protected $resourceType = 'user';

    public function getId($user)
    {
        return $user->id;
    }

    public function getAttributes($user)
    {
        return [
            'name' => $user->name,
            'surname' => $user->email,
        ];
    }

    public function getRelationships($user, $isPrimary, array $includeList)
    {
        $return = [];

        //Ability to define return data with closure although it says it won't do a query if it does not
        //exist in includeList, but it does so that why I add in_array
        //https://github.com/neomerx/json-api/wiki/Schemas#data-and-meta-closures-in-relationships
        if (in_array('articles', $includeList)) {
            $return['articles'] = [
                self::DATA => function () use ($user) {
                    return $user->articles;
                }
            ];
        }

        if (in_array('address', $includeList)) {
            //If user articles exist on user object we won't do a query
            $return['address'][self::DATA] = $user->address;
        }

        /*
        if (in_array('articles.comments', $includeList)) {
            $return['articles'] = [
                self::DATA => function () use ($user) {
                    return $user->articles;
                }
            ];
        }
        */

        if (in_array('address', $includeList)) {
            //If user articles exist on user object we won't do a query
            $include['address'][self::DATA] = $user->address;
        }

        /*
        if (in_array('articles', $includeList)) {
            //If user articles exist on user object we won't do a query
            $include['articles'][self::DATA] = $user->articles;
        }
        /*


        /*
        if (in_array('articles.comments', $includeList)) {
            //If user articles exist on user object we won't do a query
            $include['address'][self::DATA] = $user->article;
        }
        */

        return $return;
    }

    public function getIncludePaths()
    {
        return [
            //'articles.comments'
        ];
    }
}
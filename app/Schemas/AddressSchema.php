<?php

namespace App\Schemas;

use Neomerx\JsonApi\Schema\SchemaProvider;

class AddressSchema extends SchemaProvider
{
    protected $resourceType = 'address';

    public function getId($address)
    {
        return $address->id;
    }

    public function getAttributes($address)
    {
        return [
            'city' => $address->city,
            'street' => $address->street,
        ];
    }
}
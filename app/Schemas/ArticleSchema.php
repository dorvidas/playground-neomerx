<?php

namespace App\Schemas;

use Neomerx\JsonApi\Schema\SchemaProvider;

class ArticleSchema extends SchemaProvider
{
    protected $resourceType = 'article';

    public function getId($article)
    {
        return $article->id;
    }

    public function getAttributes($article)
    {
        return [
            'title' => $article->title,
            'body' => $article->body,
        ];
    }

    public function getRelationships($article, $isPrimary, array $includeList)
    {
        $return = [];
        if (in_array('comments', $includeList)) {
            $return['comments'][self::DATA] = $article->comments;
        }
        return $return;
    }
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = [
        'city',
        'street'
    ];

    public $timestamps = false;

    protected $table = 'addresses';
}

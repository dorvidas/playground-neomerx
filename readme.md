# Playing with neomerx/json-api

## Usage
* Go to `/create` to create user, article, comments, and comment's user
* Go to one of:
  * /users
  * /users?include=articles
  * /users?include=articles,address
  * /users?include=articles,address.comments
  * /users?include=articles,address.comments.user
